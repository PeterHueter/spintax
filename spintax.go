package spintax

import (
	"math/rand"
	"regexp"
	"strings"
	"time"
)

var re = regexp.MustCompile(`\{([^\{\}]+)\}`)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// Распарсить текст, поддурживаются вложения
func Process(s string) string {
	replace := func(match string) string {
		parts := strings.Split(match[1:len(match)-1], "|")
		res := parts[rand.Intn(len(parts))]
		return res
	}
	olds := s
	news := re.ReplaceAllStringFunc(s, replace)

	for olds != news {
		olds = news
		news = re.ReplaceAllStringFunc(news, replace)
	}

	return news
}
